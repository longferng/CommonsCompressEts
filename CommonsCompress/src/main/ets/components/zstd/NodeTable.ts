/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Arrays from '../util/Arrays';

export default class NodeTable {
    count: Int32Array;
    parents: Int16Array;
    symbols: Int32Array;
    numberOfBits: Int8Array;

    constructor(size: number) {
        this.count = new Int32Array(size);
        this.parents = new Int16Array(size);
        this.symbols = new Int32Array(size);
        this.numberOfBits = new Int8Array(size);
    }

    public reset(): void
    {
        Arrays.fill(this.count, 0);
        Arrays.fill(this.parents, 0);
        Arrays.fill(this.symbols, 0);
        Arrays.fill(this.numberOfBits, 0);
    }

    public copyNode(from: number, to: number): void
    {
        this.count[to] = this.count[from];
        this.parents[to] = this.parents[from];
        this.symbols[to] = this.symbols[from];
        this.numberOfBits[to] = this.numberOfBits[from];
    }
}
