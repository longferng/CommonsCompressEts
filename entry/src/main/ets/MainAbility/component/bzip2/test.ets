/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import featureAbility from '@ohos.ability.featureAbility';
import fileio from '@ohos.fileio';
import { CompressorInputStream, CompressorStreamFactory, InputStream, OutputStream, IOUtils, CompressorOutputStream
} from '@ohos/commons-compress';

@Entry
@Component
export struct Bzip2Test {
  @State isCompressBzip2FileShow: boolean = false;
  @State isDeCompressBzip2Show: boolean = false;
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('bzip2相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成hello.txt')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressBzip2FileShow) {
        Text('点击压缩hello.txt为bz2文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.bzip2FileTest()
            }
          })
      }

      if (this.isDeCompressBzip2Show) {
        Text('点击解压bz2文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.unBzip2FileTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  generateTextFile(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        const writer = fileio.openSync(data + '/hello.txt', 0o102, 0o666);
        fileio.writeSync(writer, "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011\n"
        + "111111111111111111111111111000101011"
        );
        fileio.closeSync(writer);
        AlertDialog.show({ title: '生成成功',
          message: '请查看手机路径' + data + '/hello.txt',
          confirm: { value: 'OK', action: () => {
            this.isCompressBzip2FileShow = true
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  bzip2FileTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        console.info('directory obtained. Data:' + data);
        let inputStream: InputStream = new InputStream();
        inputStream.setFilePath(data + '/hello.txt');
        let fOut: OutputStream = new OutputStream();
        fOut.setFilePath(data + '/hello.txt.bz2');
        let cos: CompressorOutputStream = new CompressorStreamFactory(false).createCompressorOutputStream("bzip2", fOut);
        IOUtils.copy(inputStream, cos);
        cos.close();
        inputStream.close();
        AlertDialog.show({ title: '压缩成功',
          message: '请查看手机路径 ' + data + '/hello.txt.gz',
          confirm: { value: 'OK', action: () => {
            this.isDeCompressBzip2Show = true
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  unBzip2FileTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        let inputStream: InputStream = new InputStream();
        inputStream.setFilePath(data + '/hello.txt.bz2');
        let fOut: OutputStream = new OutputStream();
        fOut.setFilePath(data + '/hello.txt');
        let input: CompressorInputStream = new CompressorStreamFactory(false).createCompressorInputStream2("bzip2", inputStream);
        IOUtils.copy(input, fOut);
        inputStream.close();
        fOut.close();
        AlertDialog.show({ title: '解缩成功',
          message: '请查看手机路径 ' + data + '/hello.txt',
          confirm: { value: 'OK', action: () => {
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  isFastClick(): boolean {
    var timestamp = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}